# WYMAGNAIA #

* Java 1.8 (?)
* Maven

# INSTALACA #

`mvn clean install`

# URUCHAMIANIE #

Stwórz bazę danych MySQL / MariaDB o nazwie "data_warehouse"

`mvn spring-boot:run`
lub z (java -jar/IDE)
`ETLApplication.class`

### IDE ###

Import do IDE jako "Maven Project"
