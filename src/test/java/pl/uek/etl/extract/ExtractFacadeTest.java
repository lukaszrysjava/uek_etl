package pl.uek.etl.extract;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExtractFacadeTest {

    @Autowired
    ExtractFacade extractFacade;

    @Test
    public void shouldSave_extractedProduct() {
        //given
        String productCode = "52408449";

        //when
        extractFacade.extractProduct(productCode);

        //then
        Product product = extractFacade.loadExtractedProduct(productCode);
        assertThat(product.getName()).isEqualToIgnoringCase("SAMSUNG GALAXY J5 2017 J530F DUAL SIM 16GB CZARNY");
    }

}