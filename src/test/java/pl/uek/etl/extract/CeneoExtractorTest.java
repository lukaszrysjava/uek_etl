package pl.uek.etl.extract;

import org.junit.Test;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class CeneoExtractorTest {

    @Test
    public void extractProduct() {
        //given: product with id "52408449"
        CeneoExtractor extractor = new CeneoExtractor("52408449");

        //when
        Product product = extractor.extractProduct();

        //then
        //verify product
        verifyProductData(product);

        //verify opinions
        Opinion opinion = findOpinionByAuthor(product.getOpinions(), "Oled jest najlepszy");
        verifyOpinionData(opinion);
        assertThat(product.getOpinions().size()).isGreaterThanOrEqualTo(23);
    }

    private void verifyProductData(Product product) {
        assertThat(product.getName()).isEqualToIgnoringCase("SAMSUNG GALAXY J5 2017 J530F DUAL SIM 16GB CZARNY");
        assertThat(product.getCategories()).contains("Telefony i akcesoria");
        assertThat(product.getBrand()).isEqualTo("Samsung");
        assertThat(product.getType()).isEqualTo("Smartfony");
    }

    private void verifyOpinionData(Opinion opinion) {
        assertThat(opinion.getRate()).isEqualTo("5/5");
        assertThat(opinion.getRecommendation()).isEqualToIgnoringCase("polecam");
        assertThat(opinion.getDate()).isEqualTo("2017-09-06 10:35:20");
        assertThat(opinion.getDescription()).contains("Prawdziwy Dual-Sim, czyli kartę SD można zawsze wsadzić.");
        assertThat(opinion.getPros()).contains("czas pracy na baterii");
        assertThat(opinion.getNoVotes()).isEqualTo(0);
        assertThat(opinion.getYesVotes()).isEqualTo(0);
    }

    private Opinion findOpinionByAuthor(List<Opinion> opinions, String authorName) {
        return opinions.stream()
                .filter(x -> Objects.equals(x.getAuthor(), authorName))
                .findFirst().orElse(null);
    }
}
