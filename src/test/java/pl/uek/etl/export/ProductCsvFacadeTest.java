package pl.uek.etl.export;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.uek.etl.load.LoadOpinion;
import pl.uek.etl.load.LoadProduct;
import pl.uek.etl.load.ProductRepository;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ProductCsvFacadeTest {

    @Mock
    private ProductRepository loadProductRepository;

    private ProductCsvFacade tested;

    @Before
    public void setupMock(){
        tested = new ProductCsvFacade(loadProductRepository);
    }

    @After
    public void deleteFile(){
        File directory = new File(ProductCsvFacade.FILE_LOCATION);
        try {
            FileUtils.cleanDirectory(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
        directory.delete();
    }
    
    @Test
    public void generateCsvFile() {
        //Given
        when(loadProductRepository.findAll()).thenReturn(generateRandomProducts());
        //When
        tested.generateProductFiles();
        //Then
    }

    private List<LoadProduct> generateRandomProducts() {
        List<LoadProduct> loadProducts = new LinkedList<>();
        List<LoadOpinion> loadOpinions = new LinkedList<>();
        loadOpinions.add(LoadOpinion.builder().id(12L).description("test").build());
        loadProducts.add(LoadProduct.builder().id(1L).productCode("code").name("name").opinions(loadOpinions).build());
        loadProducts.add(LoadProduct.builder().id(2L).productCode("code2").name("name2").opinions(loadOpinions).build());
        loadProducts.add(LoadProduct.builder().id(3L).productCode("code3").name("name3").opinions(loadOpinions).build());
        return loadProducts;
    }
}
