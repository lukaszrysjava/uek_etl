package pl.uek.etl.load;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class JooqRepositoriesTest extends SimpleProduct {

    @Autowired
    JooqOpinionRepository opinionRepository;

    @Autowired
    JooqProductRepository productRepository;

    @Test
    public void saveProductWithOpinions() {
        productRepository.save(loadProduct);

        LoadProduct savedProduct = productRepository.findOne(loadProduct.getProductCode());
        assertThat(savedProduct.getName()).isEqualTo(loadProduct.getName());
        assertThat(savedProduct.getOpinions().size()).isEqualTo(opinionsDto.size());
        assertTrue(productRepository.exists(loadProduct.getProductCode()));

        LoadOpinion opinionByCode = opinionRepository.findOneByCode(loadOpinion1.getCode());
        assertNotNull(opinionByCode);
        assertThat(opinionByCode.getDescription()).isEqualTo(loadOpinion1.getDescription());
    }

    @Test
    public void saveAndFetchAllProducts() {
        //given: 2 saved products
        LoadProduct nextProduct = LoadProduct.builder().productCode("77543").name("slipy").build();
        productRepository.save(loadProduct);
        productRepository.save(nextProduct);

        //when
        List<LoadProduct> products = productRepository.findAll();

        //then
        assertThat(products.size()).isEqualTo(2);
    }

    @Test
    public void saveTwoProductsWithSameCode_andFetchTheNewest() {
        //given: saved product
        productRepository.save(loadProduct);
        productRepository.save(modifiedProduct);

        //when
        LoadProduct newestModifiedProduct = productRepository.findOne(modifiedProduct.getProductCode());

        //then
        assertThat(newestModifiedProduct.getName()).isEqualTo(modifiedProduct.getName());
    }

    @Test
    public void saveTwoOpinionsWithSameCode_andFetchTheNewest() {
        //given: saved product with opinions and same product with modified opinion
        productRepository.save(loadProduct);
        productRepository.save(productWithModifiedOpinion);

        //when: fetch products
        LoadProduct product = productRepository.findOne(loadProduct.getProductCode());

        //then assert
        assertThat(product.getName()).isEqualTo(productWithModifiedOpinion.getName());
        assertThat(product.getOpinions().size()).isEqualTo(2);
    }
}