package pl.uek.etl.load;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProductComparisonTest extends SimpleProduct {

    @Test
    public void productWithSameValues_shouldBeEqual() {
        //when product has same values, except auto generated id
        LoadProduct sameProduct = LoadProduct.builder()
                .productCode(loadProduct.getProductCode())
                .name(loadProduct.getName())
                .opinions(opinionsDto).build();
        sameProduct.setId(888L);
        assertTrue(loadProduct.equals(sameProduct));
    }

    @Test
    public void productWithDifferentValues_shouldNotBeEqual() {
        //when product has same values, except auto generated id
        LoadProduct sameProduct = LoadProduct.builder()
                .productCode(loadProduct.getProductCode())
                .name("different name")
                .opinions(opinionsDto).build();
        assertFalse(loadProduct.equals(sameProduct));
    }

}