package pl.uek.etl.load;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class OpinionComparisonTest extends SimpleProduct {

    @Test
    public void opinionWithSameValues_shouldBeEqual() {
        //when opinion has same values, except auto generated id
        LoadOpinion sameOpinion = LoadOpinion.builder()
                .code(loadOpinion1.getCode())
                .usefulVotes(loadOpinion1.getUsefulVotes())
                .uselessVotes(loadOpinion1.getUselessVotes())
                .description(loadOpinion1.getDescription()).build();
        sameOpinion.setId(666L);
        assertTrue(loadOpinion1.equals(sameOpinion));
    }

    @Test
    public void opinionWithDifferentValues_shouldNotBeEqual() {
        //when product has same values, except auto generated id
        LoadOpinion sameOpinion = LoadOpinion.builder()
                .code(loadOpinion1.getCode())
                .description(loadOpinion1.getDescription())
                .author("lolek").build();
        assertFalse(loadOpinion1.equals(sameOpinion));
    }

}