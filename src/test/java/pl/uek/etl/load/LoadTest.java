package pl.uek.etl.load;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional(transactionManager = "loadTransactionManager")
public class LoadTest extends SimpleProduct {

    @Autowired
    private LoadFacade loadFacade;

    @Test
    public void loadNewData() {
        //when
        loadFacade.load(loadProduct);

        //and
        LoadProduct savedProduct = loadFacade.fetchProduct(loadProduct.getProductCode());

        //then
        assertThat(savedProduct.getName()).isEqualToIgnoringCase(loadProduct.getName());
        assertThat(savedProduct.getOpinions().size()).isEqualTo(2);
    }

    @Test
    public void shoulNotLoadDuplicatedOpinion() {
        //given loaded product
        loadFacade.load(loadProduct);

        //and given same product with one new opinion
        LoadOpinion newOpinion = LoadOpinion.builder().code("352463").description("Ale cuchno").build();
        List<LoadOpinion> opinionsWithOneNew = Arrays.asList(loadOpinion1, loadOpinion2, newOpinion);
        LoadProduct productWithNewOpinion = LoadProduct.builder().productCode("1027491")
                .name("Majteczki w kropeczki").opinions(opinionsWithOneNew).build();

        //when
        loadFacade.load(productWithNewOpinion);

        //then
        LoadProduct savedProduct = loadFacade.fetchProduct(productWithNewOpinion.getProductCode());
        assertThat(savedProduct.getOpinions().size()).isEqualTo(3);
    }

    @Test
    public void shouldSaveModifiedProduct() {
        //given loaded product
        loadFacade.load(loadProduct);

        //when
        loadFacade.load(modifiedProduct);

        //then
        List<LoadProduct> historyProducts = loadFacade.fetchProductHistory(modifiedProduct.getProductCode());
        assertThat(historyProducts.size()).isEqualTo(2);
    }

    @Test
    public void shouldDeleteOpinion() {
        //given loaded product
        loadFacade.load(loadProduct);

        //when load same product with one opinion deleted
        List<LoadOpinion> opinionsWithOneDeleted = new ArrayList<>(loadProduct.getOpinions());
        opinionsWithOneDeleted.remove(0);
        assertThat(opinionsWithOneDeleted.size()).isEqualTo(loadProduct.getOpinions().size()-1);
        loadProduct.setOpinions(opinionsWithOneDeleted);
        LoadResult loadResult = loadFacade.load(loadProduct);

        //then
        LoadProduct fetchedProduct = loadFacade.fetchProduct(this.loadProduct.getProductCode());
        assertThat(fetchedProduct.getOpinions().size()).isEqualTo(loadProduct.getOpinions().size());
        assertThat(loadResult.getDeletedOpinions()).isEqualTo(1);
    }
}
