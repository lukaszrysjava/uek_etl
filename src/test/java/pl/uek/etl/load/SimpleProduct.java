package pl.uek.etl.load;

import java.util.Arrays;
import java.util.List;

class SimpleProduct {

    LoadOpinion loadOpinion1 = LoadOpinion.builder().code("452").description("Fajne").uselessVotes(1).usefulVotes(2).build();
    LoadOpinion loadOpinion2 = LoadOpinion.builder().code("123").description("Ubieram je codziennie").build();
    List<LoadOpinion> opinionsDto = Arrays.asList(loadOpinion1, loadOpinion2);
    LoadProduct loadProduct = LoadProduct.builder()
            .productCode("23522")
            .name("Fajne majty")
            .opinions(opinionsDto).build();

    LoadProduct modifiedProduct = LoadProduct.builder()
            .productCode(loadProduct.getProductCode())
            .name("modified title")
            .opinions(opinionsDto).build();

    LoadOpinion modifiedOpinion = LoadOpinion.builder().code(loadOpinion2.getCode()).description("Modified description").build();
    List<LoadOpinion> modifiedOpinions = Arrays.asList(loadOpinion1, modifiedOpinion);
    LoadProduct productWithModifiedOpinion = LoadProduct.builder()
            .productCode(loadProduct.getProductCode())
            .name("also modified name")
            .opinions(modifiedOpinions).build();
}
