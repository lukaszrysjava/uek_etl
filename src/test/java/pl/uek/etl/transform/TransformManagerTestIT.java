package pl.uek.etl.transform;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.uek.etl.extract.ExtractFacade;
import pl.uek.etl.extract.Opinion;
import pl.uek.etl.extract.Product;
import pl.uek.etl.transform.domain.TransformOpinion;
import pl.uek.etl.transform.domain.TransformProduct;

import java.time.LocalDateTime;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransformManagerTestIT {

    @Autowired
    private ExtractFacade extractFacade;

    @Autowired
    private TransformManager transformManager;

    @Test
    public void shouldTransformExtractedProduct(){
        List<String> a = new ArrayList<>();
        Set<String> b = new TreeSet<>();

        b.stream().forEach(c -> c.toLowerCase());
        //Given
        String productCode = "52408449";
        extractFacade.extractProduct(productCode);
        Product product = extractFacade.loadExtractedProduct(productCode);
        //When
        transformManager.transformProduct(productCode);
        //Then
        TransformProduct transformProduct = transformManager.loadTransformedProduct(productCode);

        assertProduct(product, transformProduct);
        assertOpinions(product.getOpinions(), transformProduct.getOpinions());

    }

    private void assertOpinions(List<Opinion> opinions, List<TransformOpinion> transformOpinions) {
        final int size = opinions.size();
        Assert.assertEquals(size, transformOpinions.size());
        Collections.sort(opinions, opinionComperator());
        Collections.sort(transformOpinions, transformOpinionComperator());
        for (int i=0; i<size;i++){
            assertOpinion(opinions.get(i), transformOpinions.get(i));
        }
    }

    private void assertOpinion(Opinion c, TransformOpinion tc) {
        Assert.assertEquals(c.getId(), tc.getId());
        assertSet(c.getPros(), tc.getPros());
        assertSet(c.getCons(), tc.getCons());
        Assert.assertEquals(c.getAuthor(), tc.getAuthor());
        Assert.assertEquals(c.getDescription(), tc.getDescription());
        Assert.assertEquals(c.getRecommendation(), tc.getRecommendation());
        Assert.assertTrue(c.getRate().contains(tc.getRate()));
        assertDate(c.getDate(), tc.getDate());
    }

    private void assertDate(String date, LocalDateTime localDateTime) {
        date.contains(String.valueOf(localDateTime.getYear()));
        date.contains(String.valueOf(localDateTime.getMonth()));
        date.contains(String.valueOf(localDateTime.getDayOfMonth()));
        date.contains(String.valueOf(localDateTime.getSecond()));
        date.contains(String.valueOf(localDateTime.getMinute()));
        date.contains(String.valueOf(localDateTime.getHour()));
    }


    private void assertProduct(Product product, TransformProduct transformProduct) {
        Assert.assertEquals(product.getName(), transformProduct.getName());
        Assert.assertEquals(product.getBrand(), transformProduct.getBrand());
        assertSet(product.getCategories(), transformProduct.getCategories());
    }

    private void assertSet(String origin, Set<String> wrapped){
        wrapped.forEach((c) -> Assert.assertTrue(origin.contains(c)));
    }

    private Comparator<Opinion> opinionComperator() {
        return (left, right) -> {
            if (left.getId().equals(right.getId())){
                return -1;
            }
            return left.getId().compareTo(right.getId());
        };
    }

    private Comparator<TransformOpinion> transformOpinionComperator() {
        return (left, right) -> {
            if (left.getId().equals(right.getId())){
                return -1;
            }
            return left.getId().compareTo(right.getId());
        };
    }
}
