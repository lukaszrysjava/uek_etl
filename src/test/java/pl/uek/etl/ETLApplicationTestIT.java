package pl.uek.etl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.etl.extract.ExtractFacade;
import pl.uek.etl.load.LoadFacade;
import pl.uek.etl.load.LoadProduct;
import pl.uek.etl.load.ProductRepository;
import pl.uek.etl.transform.TransformManager;
import pl.uek.etl.transform.domain.TransformProduct;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ETLApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Transactional
public class ETLApplicationTestIT {

    @Autowired
    ExtractFacade extractFacade;

    @Autowired
    TransformManager transformManager;

    @Autowired
    LoadFacade loadFacade;

    @Autowired
    ProductRepository productRepository;


    @Test
    public void endToEndTest() {
        String productCode = "50351320";
        extractFacade.extractProduct(productCode);
        transformManager.transformProduct(productCode);
        TransformProduct transformedProduct = transformManager.loadTransformedProduct(productCode);
        loadFacade.load(productCode);

        //when loaded on more time
        extractFacade.extractProduct(productCode);
        transformManager.transformProduct(productCode);
        loadFacade.load(productCode);

        //then
        List<LoadProduct> productHistory = productRepository.findProductHistory(productCode);
        //product has not changed so was not inserted again
        assertThat(productHistory.size()).isEqualTo(1);
        //opinions was same so opinions were not inserted again
        assertThat(transformedProduct.getOpinions().size()).isEqualTo(productHistory.get(0).getOpinions().size());
    }
}
