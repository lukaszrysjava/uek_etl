package pl.uek.etl.export;

import pl.uek.etl.load.LoadOpinion;
import pl.uek.etl.load.LoadProduct;
import pl.uek.etl.load.ProductRepository;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class resposible for implementing CSV creation for product.
 */
class OpinionCsvWriter extends AbstractCsvWriter {

    private final ProductRepository loadProductRepository;

    public OpinionCsvWriter(String fileName, ProductRepository loadProductRepository) {
        super(fileName);
        this.loadProductRepository = loadProductRepository;
    }

    /** Fill data in CSV file.
     * @throws IOException
     */
    @Override
    protected void fillData() {
        loadProductRepository.findAll().forEach(p -> p.getOpinions().forEach(o -> extractOpinionData(o)));
    }

    /** Creates headers in CSV file.
     * @throws IOException
     */
    @Override
    protected void fillHeaders() {
        addEntry("author", false);
        addEntry("rate", false);
        addEntry("descritpion", false);
        addEntry("cons", false);
        addEntry("pros", false);
        addEntry("date", true);
    }

    private void extractOpinionData(LoadOpinion opinion){
        addEntry(opinion.getAuthor(), false);
        addEntry(opinion.getRate(), false);
        addEntry(opinion.getDescription(), false);
        if (opinion.getCons() != null) {
            addEntry(opinion.getCons().stream().map(c -> c.toString()).collect(Collectors.joining(",")), false);
        }
        if (opinion.getPros() != null) {
            addEntry(opinion.getPros().stream().map(c -> c.toString()).collect(Collectors.joining(",")), false);
        }
        if (opinion.getDate() != null) {
            addEntry(opinion.getDate().toString(), true);
        } else {
            addEntry("", true);
        }
    }
}
