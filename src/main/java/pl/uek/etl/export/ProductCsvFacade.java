package pl.uek.etl.export;

import lombok.AllArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import pl.uek.etl.load.ProductRepository;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This is the main class responsible for generating and downloading CSV and text files.
 */
@Service
@AllArgsConstructor
public class ProductCsvFacade {
    private final ProductRepository repository;

    public static final String FILE_LOCATION = "generated-files";
    private static final String FILE_NAME = "/products";
    private static final String OPINION_FILE_NAME = "/opinions";

    /**
     * Generates files for all products in the system.
     */
    @Async
    public void generateProductFiles() {
        if (!checkIfFileLocationExists()) {
            createFileLocation();
        }
        clearFileLocation();
        final ExecutorService executorService = Executors.newFixedThreadPool(6);
        executorService.submit(new ProductCsvWriter(FILE_LOCATION + FILE_NAME, repository));
        executorService.submit(new OpinionCsvWriter(FILE_LOCATION + OPINION_FILE_NAME,repository));
        executorService.submit(new OpinionFileWriter(FILE_LOCATION, repository));
    }

    private void clearFileLocation() {
        try {
            FileUtils.cleanDirectory(new File(FILE_LOCATION));
        } catch (IOException e) {
            throw new RuntimeException("Directory has not been created.");
        }
    }


    /** Returns ZIP file that contains csv and text files.
     * @return
     */
    public File downloadProductCsvFile() {
        try {
            if (!checkIfFileLocationExists()) {
                throw new RuntimeException(String.format("Brak wygenerowanych plików. Wygeneruj najpierw pliki lub pczekaj na ich wyegenrowanie."));
            }
            return new ZipFileHelper().zipIt(FILE_LOCATION);
        } catch (IOException e) {
            throw new RuntimeException("Directory has not been created.");
        }
    }

    private boolean checkIfFileLocationExists(){
        return new File(FILE_LOCATION).exists();
    }
    private void createFileLocation() {
        new File(FILE_LOCATION).mkdir();
    }
}
