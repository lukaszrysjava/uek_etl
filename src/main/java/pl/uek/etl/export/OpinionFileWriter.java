package pl.uek.etl.export;

import pl.uek.etl.load.LoadOpinion;
import pl.uek.etl.load.ProductRepository;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Class resposible for creating text files for all opinions in the database.
 */
public class OpinionFileWriter implements Runnable{

    protected static final String NEW_LINE_SEPERATOR = "\n";

    private String directory;
    private ProductRepository productRepository;

    public OpinionFileWriter(String directory, ProductRepository productRepository) {
        this.directory = directory;
        this.productRepository = productRepository;
    }

    /**
     * Initilizes creation of .txt files for all opinions in given directory. The name of file is productId-opinionId.txt.
     */
    @Override
    public void run() {
        productRepository.findAll().forEach(p -> p.getOpinions().forEach(o -> createOpinionFile(o, p.getId())));
    }

    private void createOpinionFile(LoadOpinion opinion, Long productId) {
        Writer fileWriter = null;
        try {
            StringBuilder fileName = fileName(opinion.getCode(), productId);
            fileWriter = new OutputStreamWriter(new FileOutputStream(fileName.toString()), StandardCharsets.UTF_8);
            fillNewOpinionFile(opinion, fileWriter);
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.flush();
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private StringBuilder fileName(String opinionId, Long productId) {
        StringBuilder fileName = new StringBuilder(directory);
        fileName.append("/");
        fileName.append(productId);
        fileName.append("-");
        fileName.append(opinionId);
        fileName.append(".txt");
        return fileName;
    }

    private void fillNewOpinionFile(LoadOpinion opinion, Writer fileWriter) throws IOException {
        appendSafely("Autor:",opinion.getAuthor(), fileWriter);
        appendSafely("Ocena:",opinion.getRate(), fileWriter);
        appendSafely("Opis:",opinion.getDescription(), fileWriter);
        appendSafely("Wady:",opinion.getCons(), fileWriter);
        appendSafely("Zalety:",opinion.getPros(), fileWriter);
        appendSafely("Data:",opinion.getDate(), fileWriter);
    }

    private void appendSafely(String label, Object content, Writer fileWriter) throws IOException{
        if (content != null) {
            fileWriter.append(label);
            fileWriter.append(content.toString());
            fileWriter.append(";");
            newLine(fileWriter);
        }
    }
    private void newLine(Writer fileWriter) throws IOException {
        fileWriter.append(NEW_LINE_SEPERATOR);
    }
}
