package pl.uek.etl.export;

import pl.uek.etl.load.LoadOpinion;
import pl.uek.etl.load.LoadProduct;
import pl.uek.etl.load.ProductRepository;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class resposible for implementing CSV creation for product.
 */
class ProductCsvWriter extends AbstractCsvWriter {

    private final ProductRepository loadProductRepository;

    public ProductCsvWriter(String fileName, ProductRepository loadProductRepository) {
        super(fileName);
        this.loadProductRepository = loadProductRepository;
    }

    /** Fill data in CSV file.
     * @throws IOException
     */
    @Override
    protected void fillData() {
        final List<LoadProduct> allProducts = loadProductRepository.findAll();
        for (LoadProduct product : allProducts){
            extractProductData(product);
        }
    }

    /** Creates headers in CSV file.
     * @throws IOException
     */
    @Override
    protected void fillHeaders()  {
        addEntry("productCode", false);
        addEntry("name", false);
        addEntry("brand", false);
        addEntry("type", false);
        addEntry("categories", false);
        addEntry("noAmount", false);
        addEntry("yesAmount", true);
    }

    private void extractProductData(LoadProduct product) {
        addEntry(product.getProductCode(), false);
        addEntry(product.getName(), false);
        addEntry(product.getBrand(), false);
        addEntry(product.getType(), false);
        if (product.getCategories() != null) {
            addEntry(product.getCategories().stream().map(c -> c.toString()).collect(Collectors.joining(",")), false);
        }
        addEntry(product.getNoAmount() != null ? product.getNoAmount().toString() : "0", false);
        addEntry(product.getYesAmount() != null ? product.getYesAmount().toString() : "0", true);
    }
}
