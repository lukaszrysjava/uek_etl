package pl.uek.etl.export;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.locks.StampedLock;

/**
 * Abstract class thats resposible for creating any CSV file.
 */
abstract class AbstractCsvWriter implements Runnable{
    
    protected String fileName;
    protected static final String COMMA_DELIMITER = ",";
    protected static final String NEW_LINE_SEPERATOR = "\n";

    private Writer fileWriter;
    
    public AbstractCsvWriter(String fileName){
        this.fileName = fileName;
        this.fileWriter = null;
    }

    /**
     * Initialize creation of CSV file with given fileName in constructor.
     */
    public void run(){
        StampedLock lock = new StampedLock();
        try {
            initFileWriter(lock);
            initFillData(lock);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeFileWriter();
        }
    }

    protected abstract void fillData() throws IOException;

    /** Adds entry to the file and creates newLine if true.
     * @param entry
     * @param newLine
     * @throws IOException
     */
    protected void addEntry(String entry, boolean newLine) {
        try {
            fileWriter.append(entry);
            fileWriter.append(newLine ? NEW_LINE_SEPERATOR : COMMA_DELIMITER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeFileWriter() {
        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void initFillData(StampedLock lock) throws IOException{
        final long writeStamp = lock.writeLock();
        try {
            fillHeaders();
            fillData();
        } finally {
            lock.unlockWrite(writeStamp);
        }
    }

    protected abstract void fillHeaders()  throws IOException;

    private void initFileWriter(StampedLock lock) throws IOException {
        final long readStamp = lock.readLock();
        try {

            fileWriter = new OutputStreamWriter(new FileOutputStream(fileName + ".csv"), StandardCharsets.UTF_8);
        } finally {
            lock.unlockRead(readStamp);
        }
    }
}
