package pl.uek.etl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Main class responsible for running project
 */
@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class})
@EnableTransactionManagement
public class ETLApplication {

    /**
     * Main method responsible for running project
     *
     * @param args run arguments
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ETLApplication.class, args);
    }

}
