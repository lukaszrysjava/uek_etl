package pl.uek.etl.transform;

import org.springframework.stereotype.Component;
import pl.uek.etl.extract.Opinion;
import pl.uek.etl.transform.domain.TransformOpinion;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;


@Component
class OpinionTransformer implements Transformer {

    public List<TransformOpinion> transform(List<Opinion> originList) {
        final List<TransformOpinion> list = new LinkedList<>();
        originList.forEach((o) -> list.add(transform(o)));
        return list;
    }

    private TransformOpinion transform(Opinion origin) {
        return TransformOpinion.builder()
                .id(origin.getId())
                .rate(transformRate(origin.getRate()))
                .author(removeBadChars(origin.getAuthor()))
                .recommendation(origin.getRecommendation())
                .description(removeBadChars(origin.getDescription()))
                .date(transformDate(origin.getDate()))
                .pros(transformStringToSet(removeBadChars(origin.getPros())))
                .cons(transformStringToSet(removeBadChars(origin.getCons())))
                .usefulVotes(origin.getYesVotes())
                .uselessVotes(origin.getNoVotes())
                .build();
    }

    private String removeBadChars(String s) {
        if (s == null) return null;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (Character.isHighSurrogate(s.charAt(i))) continue;
            sb.append(s.charAt(i));
        }
        return sb.toString();
    }

    private LocalDateTime transformDate(String date) {
        DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(date, DATEFORMATTER);
    }

    private String transformRate(String rate) {
        return rate.substring(0, rate.indexOf('/'));
    }
}
