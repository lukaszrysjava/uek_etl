package pl.uek.etl.transform.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is used in transform process and contains information about opinion from Ceneo website:
 * code of opinion, code of product to which opinion belongs, rate, author, recommendation, description,
 * advantages and disadvantages of the opinion, date of creation of the opinion,
 * how many people found the opinion useful and how unhelpful it was.
 * Class is an entity and is managed by JPA specification
 */
@Entity
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransformOpinion {
    @Id
    private String id;
    private String rate;
    private String author;
    private String recommendation;
    @Column(columnDefinition = "text")
    private String description;
    @Lob
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> pros = new HashSet<>();
    @Lob
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> cons = new HashSet<>();
    private LocalDateTime date;
    private int usefulVotes;
    private int uselessVotes;
}
