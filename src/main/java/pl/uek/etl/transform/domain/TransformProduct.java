package pl.uek.etl.transform.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class is used in transform process and contains information about product from Ceneo website:
 * code of product, name, categories, brand, type, opinions, amount of No and Yes votes.
 * Class is an entity and is managed by JPA specification
 */
@Entity
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TransformProduct {

    @Id
    private String productCode;
    private String name;
    @ElementCollection
    private Set<String> categories = new HashSet<>();
    private String brand;
    private String type;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private List<TransformOpinion> opinions = new ArrayList<>();
    private int noAmount = 0;
    private int yesAmount = 0;

}
