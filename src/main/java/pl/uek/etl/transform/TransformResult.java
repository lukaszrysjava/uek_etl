package pl.uek.etl.transform;

import pl.uek.etl.transform.domain.TransformProduct;

/**
 * This class contains statistics coming from transform process
 */
public class TransformResult {
    private final String productName;
    private final String productCode;
    private final int opinionsSize;

    TransformResult(TransformProduct product) {
        productName = product.getName();
        productCode = product.getProductCode();
        opinionsSize = product.getOpinions().size();
    }

    /**
     * Creates message with statistics of finished transform process
     *
     * @return message about statistics of finished transform process
     */
    @Override
    public String toString() {
        return String.format("W procesie TRANSFORM został przetworzony produkt %s o kodzie %s\n. Produkt posiadał %d " +
                        "opinii.",
                productName, productCode, opinionsSize);
    }
}
