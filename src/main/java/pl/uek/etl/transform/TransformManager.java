package pl.uek.etl.transform;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.etl.extract.ExtractFacade;
import pl.uek.etl.extract.Product;
import pl.uek.etl.transform.domain.TransformProduct;
import pl.uek.etl.transform.exception.ExtractedProductNotFoundException;

/**
 * This is the main input class responsible for TRANSFORM process
 */
@Service
public class TransformManager {

    private TransformProductRepository transformProductRepository;
    private ExtractFacade extractFacade;
    private ProductTransformer productTransformer;

    TransformManager(TransformProductRepository transformProductRepository, ExtractFacade extractFacade,
                     ProductTransformer productTransformer) {
        this.transformProductRepository = transformProductRepository;
        this.extractFacade = extractFacade;
        this.productTransformer = productTransformer;
    }

    /**
     * This is main input method for transform process.
     * Transformer is consuming data coming from extract process and transform them for laod process.
     * After transform data is saved to embedded database.
     *
     * @param productCode code of product. This code is the same code as in the link on Ceneo website
     * @return class containing statistics about finished extract process
     */
    public TransformResult transformProduct(String productCode) {
        final TransformProduct transformedProduct = productTransformer.transform(loadExtractedProduct(productCode));
        transformProductRepository.save(transformedProduct);
        extractFacade.deleteExtractedProduct(productCode);
        return new TransformResult(transformedProduct);
    }

    /**
     * Fetches transformed product from database
     *
     * @param productCode Code of product to find
     * @return product
     */
    @Transactional("transformTransactionManager")
    public TransformProduct loadTransformedProduct(String productCode) {
        final TransformProduct product = transformProductRepository.findOne(productCode);
        if (product == null)
            throw new RuntimeException(String.format("Nie ma produktu o kodzie %s w bazie procesu TRANSFORM",
                    productCode));
        product.getCategories().size();
        product.getOpinions().size();
        return product;
    }

    private Product loadExtractedProduct(String productCode) {
        final Product extractedProduct = extractFacade.loadExtractedProduct(productCode);
        if (extractedProduct == null) {
            throw new ExtractedProductNotFoundException("Nie istnieje produkt o kodzie " + productCode + " w bazie " +
                    "procesu EXTRACT");
        }
        return extractedProduct;
    }

    /**
     * Removes product from transform database
     *
     * @param productCode code of product
     */
    public void deleteProduct(String productCode) {
        transformProductRepository.delete(productCode);
    }
}
