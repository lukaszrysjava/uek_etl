package pl.uek.etl.transform;

import java.util.HashSet;
import java.util.Set;

interface Transformer {
    default Set<String> transformStringToSet(String content) {
        Set<String> wrappedContent = new HashSet<>();
        final String[] split = content.split(";");
        for (int i=0; i<split.length;i++){
            wrappedContent.add(split[i]);
        }
        return wrappedContent;
    }
}
