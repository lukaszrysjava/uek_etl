package pl.uek.etl.transform.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Specific exception for not found product when extract API is called
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ExtractedProductNotFoundException extends RuntimeException {

    /**
     * Constructor for exception
     *
     * @param message message of exception
     */
    public ExtractedProductNotFoundException(String message) {
        super(message);
    }
}
