package pl.uek.etl.transform;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.uek.etl.transform.domain.TransformProduct;

@Repository
interface TransformProductRepository extends CrudRepository<TransformProduct, String> {
}
