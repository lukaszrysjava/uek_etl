package pl.uek.etl.transform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.uek.etl.extract.Product;
import pl.uek.etl.transform.domain.TransformProduct;

import java.util.HashSet;
import java.util.Set;

@Component
class ProductTransformer implements Transformer{

    private OpinionTransformer opinionTransformer;

    public ProductTransformer(@Autowired OpinionTransformer opinionTransformer){
        this.opinionTransformer = opinionTransformer;
    }

    public TransformProduct transform(Product origin){
        return TransformProduct.builder()
                .productCode(origin.getProductCode())
                .name(origin.getName())
                .brand(origin.getBrand())
                .type(origin.getType())
                .categories(transformStringToSet(origin.getCategories()))
                .opinions(opinionTransformer.transform(origin.getOpinions()))
                .build();
    }
}
