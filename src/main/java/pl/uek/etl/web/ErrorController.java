package pl.uek.etl.web;

import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
class ErrorController {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ErrorController.class);

    @ExceptionHandler(value = Exception.class)
    public String exception(final Throwable throwable, final Model model) {
        logger.error("Błąd - cóż żeś uczynił?", throwable);
        String errorMessage = (throwable != null ? throwable.getMessage() : "Wystąpił nieznany błąd");
        model.addAttribute("errorMessage", errorMessage);
        return "index";
    }

}
