package pl.uek.etl.web;

import lombok.AllArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.uek.etl.export.ProductCsvFacade;

import javax.servlet.http.HttpServletResponse;

import static pl.uek.etl.web.HomeController.HOME_PAGE;

@Controller
@AllArgsConstructor
public class CsvController {
    private final ProductCsvFacade productCsvFacade;

    @PostMapping(value = "/", params = "action=generate")
    public String generate(Model model) {
        productCsvFacade.generateProductFiles();
        model.addAttribute("successMsg", "Rozpoczęto eksport produktów i opinii.");
        return HOME_PAGE;
    }

    @GetMapping(value = "/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response) {
        response.setHeader("Content-Disposition", "attachment; filename=productsAndOpinions.zip");
        return new FileSystemResource(productCsvFacade.downloadProductCsvFile());
    }

}
