package pl.uek.etl.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.uek.etl.load.LoadProduct;
import pl.uek.etl.load.ProductRepository;

import java.util.List;

@Controller
@RequestMapping("/products")
class ProductController {
    private static final String PRODUCTS_PAGE = "products";
    private static final String SINGLE_PRODUCT_PAGE = "product";

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping("")
    public String products(Model model) {
        List<LoadProduct> products = productRepository.findAll();
        model.addAttribute("products", products);
        return PRODUCTS_PAGE;
    }

    @GetMapping("{productCode}")
    public String product(@PathVariable String productCode, Model model) {
        LoadProduct product = productRepository.findOne(productCode);
        model.addAttribute("product", product);
        return SINGLE_PRODUCT_PAGE;
    }
}
