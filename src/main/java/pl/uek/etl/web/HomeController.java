package pl.uek.etl.web;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import pl.uek.etl.export.ProductCsvFacade;
import pl.uek.etl.extract.ExtractFacade;
import pl.uek.etl.extract.ExtractResult;
import pl.uek.etl.load.LoadFacade;
import pl.uek.etl.load.LoadResult;
import pl.uek.etl.transform.TransformManager;
import pl.uek.etl.transform.TransformResult;

@Controller
@AllArgsConstructor
class HomeController {
    static final String HOME_PAGE = "index";

    private final ExtractFacade extractFacade;
    private final TransformManager transformManager;
    private final LoadFacade loadFacade;

    @GetMapping("/")
    public String home() {
        return HOME_PAGE;
    }

    @PostMapping(value = "/", params = "action=extract")
    public String extract(@RequestParam("productCode") String productCode, Model model) {
        ExtractResult result = extractFacade.extractProduct(productCode);
        model.addAttribute("successMsg", result.toString());
        return HOME_PAGE;
    }

    @PostMapping(value = "/", params = "action=transform")
    public String transform(@RequestParam("productCode") String productCode, Model model) {
        TransformResult transformResult = transformManager.transformProduct(productCode);
        model.addAttribute("successMsg", transformResult.toString());
        return HOME_PAGE;
    }

    @PostMapping(value = "/", params = "action=load")
    public String load(@RequestParam("productCode") String productCode, Model model) {
        LoadResult loadResult = loadFacade.load(productCode);
        model.addAttribute("successMsg", loadResult.toString());
        return HOME_PAGE;
    }

    @PostMapping(value = "/", params = "action=etl")
    public String etl(@RequestParam("productCode") String productCode, Model model) {
        ExtractResult extractResult = extractFacade.extractProduct(productCode);
        TransformResult transformResult = transformManager.transformProduct(productCode);
        LoadResult loadResult = loadFacade.load(productCode);
        String successMsg = String.format("%s\n%s\n%s", extractResult.toString(), transformResult.toString(),
                loadResult.toString());
        model.addAttribute("successMsg", successMsg);
        return HOME_PAGE;
    }

    @PostMapping(value = "/deleteAll")
    public RedirectView deleteAll(RedirectAttributes model) {
        loadFacade.deleteAll();
        model.addFlashAttribute("successMsg", "Baza danych została wyczyszczona!");
        return new RedirectView("/");
    }
}
