package pl.uek.etl.load;

import java.util.Collection;

/**
 * Interface for opinion repository.
 * Repository allows to search, save and delete opinions in laod database
 */
public interface OpinionRepository {

    /**
     * Fetches newest opinions. Old, modified opinions will not be fetched.
     *
     * @param productCode Code of product
     * @return list of opinions
     */
    Collection<LoadOpinion> fetchOpinions(String productCode);

    /**
     * Save list of opinions and assigns them to passed product
     *
     * @param product        Product for which opinions will be saved
     * @param opinionsToSave Opinions which will be saved to database
     */
    void saveOpinions(LoadProduct product, Collection<LoadOpinion> opinionsToSave);

    /**
     * Finds one newest opinion
     *
     * @param code Code of opinion to find
     * @return opinion
     */
    LoadOpinion findOneByCode(String code);

    /**
     * All opinions (old and new if have been modifies) which belongs to passed product
     *
     * @param productCode Code of product for which opinions will by search
     * @return list of opinions
     */
    Collection<LoadOpinion> fetchHistoryOpinions(String productCode);

    /**
     * Removes all products and opinions from laod database
     */
    void deleteAllOpinions();
}
