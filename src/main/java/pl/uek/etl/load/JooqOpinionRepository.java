package pl.uek.etl.load;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.uek.etl.load.model.tables.records.OpinionRecord;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import static pl.uek.etl.load.model.tables.Opinion.OPINION;

@Repository
class JooqOpinionRepository implements OpinionRepository {

    private final DSLContext dsl;

    @Autowired
    public JooqOpinionRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public Collection<LoadOpinion> fetchOpinions(String productCode) {
        /*
        SELECT * from opinion
            where id IN (SELECT MAX(id) FROM `opinion` where product_code = :productCode GROUP by code )
            and active = 1
         */
        List<OpinionRecord> opinionRecords = dsl
                .selectFrom(OPINION)
                .where(OPINION.ID.in(
                        dsl.select(OPINION.ID.max())
                                .from(OPINION)
                                .where(OPINION.PRODUCT_CODE.eq(productCode))
                                .groupBy(OPINION.CODE).fetch()))
                .and(OPINION.ACTIVE.eq((byte) 1))
                .fetch().into(OpinionRecord.class);

        return LoadOpinion.createOpinions(opinionRecords);
    }

    @Override
    public Collection<LoadOpinion> fetchHistoryOpinions(String productCode) {
        List<OpinionRecord> historyOinions = dsl.selectFrom(OPINION)
                .where(OPINION.PRODUCT_CODE.eq(productCode))
                .fetch().into(OpinionRecord.class);
        return LoadOpinion.createOpinions(historyOinions);
    }

    @Override
    public void deleteAllOpinions() {
        dsl.delete(OPINION).execute();
    }

    @Override
    public void saveOpinions(LoadProduct productFromDB, Collection<LoadOpinion> opinionsToSave) {
        if (opinionsToSave != null) {
            for (LoadOpinion opinion : opinionsToSave) {
                insertOpinion(productFromDB.getProductCode(), opinion);
            }
        }
    }

    private void insertOpinion(String productCode, LoadOpinion opinion) {
        dsl.insertInto(OPINION)
                .set(OPINION.CODE, opinion.getCode())
                .set(OPINION.PRODUCT_CODE, productCode)
                .set(OPINION.RATE, opinion.getRate())
                .set(OPINION.AUTHOR, opinion.getAuthor())
                .set(OPINION.RECOMMENDATION, opinion.getRecommendation())
                .set(OPINION.DESCRIPTION, opinion.getDescription())
                .set(OPINION.PROS, opinion.getPros() != null ? String.join(";", opinion.getPros()) : null)
                .set(OPINION.CONS, opinion.getPros() != null ? String.join(";", opinion.getCons()) : null)
                .set(OPINION.DATE, opinion.getDate() != null ? Timestamp.valueOf(opinion.getDate()) : null)
                .set(OPINION.USEFUL_VOTES, opinion.getUsefulVotes())
                .set(OPINION.USELESS_VOTES, opinion.getUselessVotes())
                .set(OPINION.ACTIVE, opinion.getActive() ? (byte) 1 : (byte) 0)
                .execute();
    }

    @Override
    public LoadOpinion findOneByCode(String code) {
        OpinionRecord opinionRecord = dsl.selectFrom(OPINION)
                .where(OPINION.CODE.eq(code))
                .orderBy(OPINION.ID.desc())
                .limit(1)
                .fetchOne()
                .into(OpinionRecord.class);

        return LoadOpinion.createOpinion(opinionRecord);
    }
}
