package pl.uek.etl.load;

import java.util.List;

/**
 * Interface for product repository.
 * Repository allows to search, save and delete products in laod database
 */
public interface ProductRepository {

    /**
     * Finds all products
     *
     * @return list of products
     */
    List<LoadProduct> findAll();

    /**
     * Finds one newest product
     *
     * @param productCode Code of product to find
     * @return product
     */
    LoadProduct findOne(String productCode);

    /**
     * Checks if product with given code exists in database
     *
     * @param productCode Code of product to search
     * @return true or false
     */
    boolean exists(String productCode);

    /**
     * Saves product to database
     *
     * @param loadProduct product
     */
    void save(LoadProduct loadProduct);

    /**
     * All prducts old and new if have been modifies
     *
     * @param productCode Code of product for which opinions will by search
     * @return list of products
     */
    List<LoadProduct> findProductHistory(String productCode);

    /**
     * Saves product to database without its opinions
     *
     * @param product product
     */
    void saveWithoutOpinions(LoadProduct product);

    /**
     * Removes all products and opinions from database saved in load process
     */
    void deleteAll();
}
