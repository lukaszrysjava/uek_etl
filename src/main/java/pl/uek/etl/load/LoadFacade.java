package pl.uek.etl.load;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uek.etl.transform.TransformManager;
import pl.uek.etl.transform.domain.TransformProduct;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This is the main input class responsible for LOAD process
 */
@Service
public class LoadFacade {

    private final ProductRepository jooqProductRepository;
    private final OpinionRepository jooqOpinionRepository;
    private final TransformManager transformManager;

    LoadFacade(JooqProductRepository jooqProductRepository, JooqOpinionRepository jooqOpinionRepository,
               TransformManager transformManager) {
        this.jooqProductRepository = jooqProductRepository;
        this.jooqOpinionRepository = jooqOpinionRepository;
        this.transformManager = transformManager;
    }

    /**
     * This is main input method for load process.
     * Firstly it translates data from transform process to load process.
     * Then it checks product and opinions are they already in database or are they have been modifies and saves them.
     * Finally after successfully laod process it calls transform API to delete data from transform process
     *
     * @param productCode code of product. This code is the same code as in the link on Ceneo website
     * @return class containing statistics about finished load process
     */
    public LoadResult load(String productCode) {
        TransformProduct transformedProduct = transformManager.loadTransformedProduct(productCode);
        LoadProduct product = LoadProduct.createProduct(transformedProduct);
        LoadResult loadResult = load(product);
        transformManager.deleteProduct(productCode);
        return loadResult;
    }

    @Transactional(transactionManager = "loadTransactionManager")
    LoadResult load(LoadProduct productToLoad) {
        if (jooqProductRepository.exists(productToLoad.getProductCode())) {
            LoadProduct productFromDB = jooqProductRepository.findOne(productToLoad.getProductCode());
            if (productIsModified(productToLoad, productFromDB)) {
                jooqProductRepository.saveWithoutOpinions(productToLoad);
            }
            int savedOpinionsSize = saveChangedOpinions(productToLoad, productFromDB);
            int deletedOpinionsSize = saveDeletedOpinions(productToLoad);

            return LoadResult.fromExistingProduct(productToLoad, savedOpinionsSize, deletedOpinionsSize);
        } else {
            jooqProductRepository.save(productToLoad);
            return LoadResult.fromNewProduct(productToLoad);
        }
    }

    LoadProduct fetchProduct(String productCode) {
        return jooqProductRepository.findOne(productCode);
    }

    List<LoadProduct> fetchProductHistory(String productCode) {
        return jooqProductRepository.findProductHistory(productCode);
    }

    private boolean productIsModified(LoadProduct newProduct, LoadProduct oldProductFromDB) {
        return !newProduct.equals(oldProductFromDB);
    }

    private int saveChangedOpinions(LoadProduct product, LoadProduct productFromDB) {
        List<LoadOpinion> opinionsToSave = new ArrayList<>();
        Collection<LoadOpinion> opinionsFromDB = jooqOpinionRepository.fetchOpinions(productFromDB.getProductCode());
        for (LoadOpinion opinion : product.getOpinions()) {
            LoadOpinion opinionFromDB = findOpinionInCollection(opinionsFromDB, opinion.getCode());
            if (opinionFromDB == null || !opinion.equals(opinionFromDB)) {
                opinionsToSave.add(opinion);
            }
        }
        jooqOpinionRepository.saveOpinions(productFromDB, opinionsToSave);
        return opinionsToSave.size();
    }

    private int saveDeletedOpinions(LoadProduct product) {
        List<LoadOpinion> opinionsToDelete = new ArrayList<>();
        Collection<LoadOpinion> opinionsFromDB = jooqOpinionRepository.fetchOpinions(product.getProductCode());
        for (LoadOpinion opinion : opinionsFromDB) {
            if (existsInCollection(product.getOpinions(), opinion.getCode())) {
                opinion.deactivate();
                opinionsToDelete.add(opinion);
            }
        }
        jooqOpinionRepository.saveOpinions(product, opinionsToDelete);
        return opinionsToDelete.size();
    }

    private boolean existsInCollection(Collection<LoadOpinion> opinionsCollection, String opinionCode) {
        return findOpinionInCollection(opinionsCollection, opinionCode) == null;
    }

    private LoadOpinion findOpinionInCollection(Collection<LoadOpinion> opinionsCollection, String opinionCode) {
        LoadOpinion opinionFromDB = null;
        for (LoadOpinion loadOpinion : opinionsCollection) {
            if (loadOpinion.getCode().equals(opinionCode))
                opinionFromDB = loadOpinion;
        }
        return opinionFromDB;
    }

    /**
     * Removes all products and opinions from database saved in load process
     */
    public void deleteAll() {
        jooqProductRepository.deleteAll();
    }
}
