package pl.uek.etl.load;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.uek.etl.load.model.tables.records.ProductRecord;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.jooq.impl.DSL.select;
import static pl.uek.etl.load.model.tables.Product.*;

@Repository
class JooqProductRepository implements ProductRepository {

    private final DSLContext dsl;
    private final OpinionRepository opinionRepository;

    @Autowired
    public JooqProductRepository(DSLContext dsl, JooqOpinionRepository opinionRepository) {
        this.dsl = dsl;
        this.opinionRepository = opinionRepository;
    }

    @Override
    public LoadProduct findOne(String productCode) {
        ProductRecord productRecord = dsl.selectFrom(PRODUCT)
                .where(PRODUCT.CODE.eq(productCode))
                .orderBy(PRODUCT.ID.desc())
                .limit(1)
                .fetchOne()
                .into(ProductRecord.class);

        Collection<LoadOpinion> opinions = opinionRepository.fetchOpinions(productCode);
        return LoadProduct.createProduct(productRecord, opinions);
    }

    @Override
    public boolean exists(String productCode) {
        return dsl.fetchExists(
                select()
                        .from(PRODUCT)
                        .where(PRODUCT.CODE.eq(productCode))
        );
    }

    @Override
    public void save(LoadProduct loadProduct) {
        saveWithoutOpinions(loadProduct);
        opinionRepository.saveOpinions(loadProduct, loadProduct.getOpinions());
    }

    @Override
    public List<LoadProduct> findProductHistory(String productCode) {
        Result<ProductRecord> products = dsl.selectFrom(PRODUCT).where(PRODUCT.CODE.eq(productCode)).fetch();
        return getProductsWithOpinions(productCode, products);
    }

    private List<LoadProduct> getProductsWithOpinions(String productCode, Result<ProductRecord> products) {
        Collection<LoadOpinion> opinions = opinionRepository.fetchHistoryOpinions(productCode);
        List<LoadProduct> finalProducts = new ArrayList<>();
        for (ProductRecord productRecord : products) {
            finalProducts.add(LoadProduct.createProduct(productRecord, opinions));
        }
        return finalProducts;
    }

    @Override
    public List<LoadProduct> findAll() {
        Result<ProductRecord> fetch = dsl.selectFrom(PRODUCT).fetch();
        List<LoadProduct> products = new ArrayList<>();
        for (ProductRecord productRecord : fetch) {
            Collection<LoadOpinion> opinions = opinionRepository.fetchOpinions(productRecord.getCode());
            products.add(LoadProduct.createProduct(productRecord, opinions));
        }
        return products;
    }

    @Override
    public void saveWithoutOpinions(LoadProduct loadProduct) {
        dsl.insertInto(PRODUCT)
                .set(PRODUCT.CODE, loadProduct.getProductCode())
                .set(PRODUCT.NAME, loadProduct.getName())
                .set(PRODUCT.CATEGORIES,
                        loadProduct.getCategories() != null ? String.join(";", loadProduct.getCategories()) : null)
                .set(PRODUCT.BRAND, loadProduct.getBrand())
                .set(PRODUCT.TYPE, loadProduct.getType())
                .set(PRODUCT.NO_AMOUNT, loadProduct.getNoAmount())
                .set(PRODUCT.YES_AMOUNT, loadProduct.getYesAmount())
                .execute();
    }

    @Override
    public void deleteAll() {
        dsl.delete(PRODUCT).execute();
        opinionRepository.deleteAllOpinions();
    }
}
