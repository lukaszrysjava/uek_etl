/**
 * This class is generated by jOOQ
 */
package pl.uek.etl.load.model.tables.records;


import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record13;
import org.jooq.Row13;
import org.jooq.impl.UpdatableRecordImpl;

import pl.uek.etl.load.model.tables.Opinion;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.8.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class OpinionRecord extends UpdatableRecordImpl<OpinionRecord> implements Record13<Integer, String, String, String, String, String, String, String, String, Timestamp, Integer, Integer, Byte> {

    private static final long serialVersionUID = 1654668736;

    /**
     * Setter for <code>data_warehouse.opinion.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>data_warehouse.opinion.code</code>.
     */
    public void setCode(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.code</code>.
     */
    public String getCode() {
        return (String) get(1);
    }

    /**
     * Setter for <code>data_warehouse.opinion.description</code>.
     */
    public void setDescription(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.description</code>.
     */
    public String getDescription() {
        return (String) get(2);
    }

    /**
     * Setter for <code>data_warehouse.opinion.product_code</code>.
     */
    public void setProductCode(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.product_code</code>.
     */
    public String getProductCode() {
        return (String) get(3);
    }

    /**
     * Setter for <code>data_warehouse.opinion.rate</code>.
     */
    public void setRate(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.rate</code>.
     */
    public String getRate() {
        return (String) get(4);
    }

    /**
     * Setter for <code>data_warehouse.opinion.author</code>.
     */
    public void setAuthor(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.author</code>.
     */
    public String getAuthor() {
        return (String) get(5);
    }

    /**
     * Setter for <code>data_warehouse.opinion.recommendation</code>.
     */
    public void setRecommendation(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.recommendation</code>.
     */
    public String getRecommendation() {
        return (String) get(6);
    }

    /**
     * Setter for <code>data_warehouse.opinion.pros</code>.
     */
    public void setPros(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.pros</code>.
     */
    public String getPros() {
        return (String) get(7);
    }

    /**
     * Setter for <code>data_warehouse.opinion.cons</code>.
     */
    public void setCons(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.cons</code>.
     */
    public String getCons() {
        return (String) get(8);
    }

    /**
     * Setter for <code>data_warehouse.opinion.date</code>.
     */
    public void setDate(Timestamp value) {
        set(9, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.date</code>.
     */
    public Timestamp getDate() {
        return (Timestamp) get(9);
    }

    /**
     * Setter for <code>data_warehouse.opinion.useful_votes</code>.
     */
    public void setUsefulVotes(Integer value) {
        set(10, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.useful_votes</code>.
     */
    public Integer getUsefulVotes() {
        return (Integer) get(10);
    }

    /**
     * Setter for <code>data_warehouse.opinion.useless_votes</code>.
     */
    public void setUselessVotes(Integer value) {
        set(11, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.useless_votes</code>.
     */
    public Integer getUselessVotes() {
        return (Integer) get(11);
    }

    /**
     * Setter for <code>data_warehouse.opinion.active</code>.
     */
    public void setActive(Byte value) {
        set(12, value);
    }

    /**
     * Getter for <code>data_warehouse.opinion.active</code>.
     */
    public Byte getActive() {
        return (Byte) get(12);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record13 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row13<Integer, String, String, String, String, String, String, String, String, Timestamp, Integer, Integer, Byte> fieldsRow() {
        return (Row13) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row13<Integer, String, String, String, String, String, String, String, String, Timestamp, Integer, Integer, Byte> valuesRow() {
        return (Row13) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Opinion.OPINION.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Opinion.OPINION.CODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Opinion.OPINION.DESCRIPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return Opinion.OPINION.PRODUCT_CODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return Opinion.OPINION.RATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field6() {
        return Opinion.OPINION.AUTHOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field7() {
        return Opinion.OPINION.RECOMMENDATION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field8() {
        return Opinion.OPINION.PROS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field9() {
        return Opinion.OPINION.CONS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field10() {
        return Opinion.OPINION.DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field11() {
        return Opinion.OPINION.USEFUL_VOTES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field12() {
        return Opinion.OPINION.USELESS_VOTES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Byte> field13() {
        return Opinion.OPINION.ACTIVE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getProductCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getRate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value6() {
        return getAuthor();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value7() {
        return getRecommendation();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value8() {
        return getPros();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value9() {
        return getCons();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value10() {
        return getDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value11() {
        return getUsefulVotes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value12() {
        return getUselessVotes();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Byte value13() {
        return getActive();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value2(String value) {
        setCode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value3(String value) {
        setDescription(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value4(String value) {
        setProductCode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value5(String value) {
        setRate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value6(String value) {
        setAuthor(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value7(String value) {
        setRecommendation(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value8(String value) {
        setPros(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value9(String value) {
        setCons(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value10(Timestamp value) {
        setDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value11(Integer value) {
        setUsefulVotes(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value12(Integer value) {
        setUselessVotes(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord value13(Byte value) {
        setActive(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpinionRecord values(Integer value1, String value2, String value3, String value4, String value5, String value6, String value7, String value8, String value9, Timestamp value10, Integer value11, Integer value12, Byte value13) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        value13(value13);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached OpinionRecord
     */
    public OpinionRecord() {
        super(Opinion.OPINION);
    }

    /**
     * Create a detached, initialised OpinionRecord
     */
    public OpinionRecord(Integer id, String code, String description, String productCode, String rate, String author, String recommendation, String pros, String cons, Timestamp date, Integer usefulVotes, Integer uselessVotes, Byte active) {
        super(Opinion.OPINION);

        set(0, id);
        set(1, code);
        set(2, description);
        set(3, productCode);
        set(4, rate);
        set(5, author);
        set(6, recommendation);
        set(7, pros);
        set(8, cons);
        set(9, date);
        set(10, usefulVotes);
        set(11, uselessVotes);
        set(12, active);
    }
}
