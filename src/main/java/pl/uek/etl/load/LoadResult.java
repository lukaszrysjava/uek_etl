package pl.uek.etl.load;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * This class contains statistics coming from load process
 */
@Getter
@Setter
@Builder
public class LoadResult {

    private String productName;
    private String productCode;
    private int savedOpinions;
    private int deletedOpinions;

    static LoadResult fromExistingProduct(LoadProduct loadProduct, int savedOpinions, int deletedOpinions) {
        return LoadResult.builder()
                .productName(loadProduct.getName())
                .productCode(loadProduct.getProductCode())
                .savedOpinions(savedOpinions)
                .deletedOpinions(deletedOpinions).build();
    }

    static LoadResult fromNewProduct(LoadProduct loadProduct) {
        return LoadResult.builder()
                .productName(loadProduct.getName())
                .productCode(loadProduct.getProductCode())
                .savedOpinions(loadProduct.getOpinions().size())
                .deletedOpinions(0).build();
    }

    /**
     * Creates message with statistics of finished load process
     *
     * @return message about statistics of finished load process
     */
    @Override
    public String toString() {
        return String.format("W procesie LOAD załadowany został produkt %s o kodzie %s\n. Do bazy zostało zapisane %d" +
                        " opinii. %d opinii zostało usuniętych",
                productName, productCode, savedOpinions, deletedOpinions);
    }
}
