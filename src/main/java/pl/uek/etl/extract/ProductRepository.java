package pl.uek.etl.extract;

import org.springframework.data.repository.CrudRepository;

/**
 * Interface for product repository.
 * Interface extends Spring Data Repository
 * Repository allows to search, save and delete products in extract database
 */
interface ProductRepository extends CrudRepository<Product, String> {

}
