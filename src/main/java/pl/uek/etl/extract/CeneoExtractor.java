package pl.uek.etl.extract;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static org.springframework.util.StringUtils.isEmpty;

class CeneoExtractor {

    private static final String JOIN_DELIMETER = ";";
    private String productCode;
    private final Document doc;

    CeneoExtractor(String productCode) {
        if (isEmpty(productCode))
            throw new RuntimeException("Kod produtu jest pusty");

        this.productCode = productCode.trim();
        try {
            String productUrl = "https://www.ceneo.pl/" + productCode;
            doc = Jsoup.connect(productUrl).get();
        } catch (IOException e) {
            throw new RuntimeException("Nie można pobrać strony CENEO", e);
        }
    }

    Product extractProduct() {
        String productName = doc.select("h1.product-name").text();
        List<String> categories = extractProductCategories();
        String brand = extractProductBrand();
        List<Opinion> opinions = extractOpinions();
        String type = getTypeFromCategories(categories);
        String joinedCategories = String.join(JOIN_DELIMETER, categories);
        return new Product(productCode, productName, joinedCategories, brand, type, opinions);
    }

    private String getTypeFromCategories(List<String> categories) {
        String type = "";
        if (!categories.isEmpty()) {
            type = categories.get(categories.size() - 1);
        }
        return type;
    }

    private String extractProductBrand() {
        Element producentEl = doc.select("#productTechSpecs table tr").select("th:contains(Producent)").first();
        if (producentEl != null)
            return producentEl.parent().children().get(1).text();
        return "";
    }

    private List<Opinion> extractOpinions() {
        List<Opinion> opinions = Collections.synchronizedList(new ArrayList<>());
        IntStream.rangeClosed(1, getNumberOfOpinionPages()).parallel().forEach(i ->
                opinions.addAll(extractOpinionsFromPage(i))
        );
        return opinions;
    }

    private List<Opinion> extractOpinionsFromPage(int numberOfPage) {
        List<Opinion> opinions = new ArrayList<>();
        String nextOpinionsUrl = "https://www.ceneo.pl/" + productCode + "/opinie-" + numberOfPage;
        try {
            Document opinionsDoc = Jsoup.connect(nextOpinionsUrl).get();
            Elements opinionElements = opinionsDoc.select(".review-box.js_product-review");

            for (Element opinionElement : opinionElements) {
                Opinion opinion = extractSingleOpinion(opinionElement);
                opinions.add(opinion);
            }
        } catch (IOException e) {
            throw new RuntimeException("Nie można pobrać kolejnej strony CENEO", e);
        }
        return opinions;
    }

    private int getNumberOfOpinionPages() {
        int numberOfOpinions = 0;
        String numberOfOpinionsStr = doc.select("span[itemprop$=reviewCount]").text();
        if (!numberOfOpinionsStr.isEmpty())
            numberOfOpinions = Integer.parseInt(numberOfOpinionsStr);
        int numberOfOpinionsPerPage = 10;
        return (int) Math.ceil(numberOfOpinions / (double) numberOfOpinionsPerPage);
    }

    private Opinion extractSingleOpinion(Element element) {
        String id = element.select("div[id^=product-review-comments-]").attr("id").replace
                ("product-review-comments-", "");
        String author = element.select(".reviewer-name-line").text();
        String rate = element.select(".review-score-count").text();
        String recommendation = element.select(".product-review-summary").text();
        String date = element.select("time").attr("datetime");
        String description = element.select(".product-review-body").first().text();
        List<String> pros = element.select(".pros-cell ul").select("li").eachText();
        List<String> cons = element.select(".cons-cell ul").select("li").eachText();
        int noVotes = Integer.parseInt(element.select(".vote-no span").text());
        int yesVotes = Integer.parseInt(element.select(".vote-yes span").text());

        String joinedCons = String.join(JOIN_DELIMETER, cons);
        String joinedPros = String.join(JOIN_DELIMETER, pros);
        return Opinion.builder()
                .id(id)
                .author(author)
                .date(date)
                .rate(rate)
                .cons(joinedCons).pros(joinedPros)
                .description(description)
                .recommendation(recommendation)
                .noVotes(noVotes).yesVotes(yesVotes)
                .build();
    }

    private List<String> extractProductCategories() {
        Elements categoriesEl = doc.select("nav.breadcrumbs .breadcrumb span");
        List<String> categories = categoriesEl.eachText();
        if (categories.get(0).equals("Ceneo"))
            categories.remove(0);
        return categories;
    }

}
