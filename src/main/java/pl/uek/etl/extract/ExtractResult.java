package pl.uek.etl.extract;

/**
 * This class contains statistics coming from extract process
 */
public class ExtractResult {
    private final String productName;
    private final String productCode;
    private final int opinionsSize;

    ExtractResult(Product product) {
        productName = product.getName();
        productCode = product.getProductCode();
        opinionsSize = product.getOpinions().size();
    }

    /**
     * Creates message with statistics of finished extract process
     *
     * @return message about statistics of finished extract process
     */
    @Override
    public String toString() {
        return String.format("W procesie EXTRACT został wyekstrakowany produkt %s o kodzie %s\n. Produkt posiadał %d " +
                        "opinii.",
                productName, productCode, opinionsSize);
    }
}
