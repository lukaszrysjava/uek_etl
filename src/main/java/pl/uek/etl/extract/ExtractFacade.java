package pl.uek.etl.extract;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * This is the main input class responsible for EXTRACT process
 */
@Service
public class ExtractFacade {

    private ProductRepository repository;

    ExtractFacade(ProductRepository repository) {
        this.repository = repository;
    }

    /**
     * This is main input method for extract process.
     * Extractor is using Jsoup library to parse data from Ceneo website.
     * After parsing data is saved to embedded database.
     *
     * @param productCode code of product. This code is the same code as in the link on Ceneo website
     * @return class containing statistics about finished extract process
     */
    public ExtractResult extractProduct(String productCode) {
        CeneoExtractor extractor = new CeneoExtractor(productCode);
        Product product = extractor.extractProduct();
        repository.save(product);
        return new ExtractResult(product);
    }

    /**
     * Fetches extracted product from database
     *
     * @param productCode Code of product to find
     * @return product
     */
    @Transactional("extractTransactionManager")
    public Product loadExtractedProduct(String productCode) {
        Product product = repository.findOne(productCode);
        if (product == null)
            throw new RuntimeException("Produkt o kodzie " + productCode + " nie istnieje w bazie procesu EXTRACT");
        // problem z lazy dlatego wywolanie size
        product.getOpinions().size();
        return product;
    }

    /**
     * Removes product from extract database
     *
     * @param productCode code of product
     */
    public void deleteExtractedProduct(String productCode) {
        repository.delete(productCode);
    }
}
