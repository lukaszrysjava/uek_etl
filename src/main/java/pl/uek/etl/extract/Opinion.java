package pl.uek.etl.extract;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.persistence.*;

/**
 * This class is used in extract process and contains information about opinion from Ceneo website:
 * code of opinion, code of product to which opinion belongs, rate, author, recommendation, description,
 * advantages and disadvantages of the opinion, date of creation of the opinion,
 * how many people found the opinion useful and how unhelpful it was.
 * Class is an entity and is managed by JPA specification
 */
@Getter
@Builder
@AllArgsConstructor
@Entity
public class Opinion {
    @Id
    private String id;

    private String rate;
    private String author;
    private String recommendation;
    private String date;
    @Lob
    @Column
    private String description;
    @Lob
    @Column
    private String pros;
    @Lob
    @Column
    private String cons;
    private int noVotes;
    private int yesVotes;

    private Opinion() {
    }
}
