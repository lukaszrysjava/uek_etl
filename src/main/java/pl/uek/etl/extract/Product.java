package pl.uek.etl.extract;

import lombok.Getter;

import javax.persistence.*;
import java.util.List;

/**
 * This class is used in extract process and contains information about product from Ceneo website:
 * code of product, name, categories, brand, type, opinions, amount of No and Yes votes.
 * Class is an entity and is managed by JPA specification
 */
@Getter
@Entity
public class Product {
    @Id
    private String productCode;
    private String name;
    private String categories;
    private String brand;
    private String type;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private List<Opinion> opinions;

    private Product() {
    }

    Product(String productCode, String name, String categories, String brand, String type, List<Opinion> opinions) {
        this.name = name;
        this.productCode = productCode;
        this.categories = categories;
        this.brand = brand;
        this.opinions = opinions;
        this.type = type;
    }
}
